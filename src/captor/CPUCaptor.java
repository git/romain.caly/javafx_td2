package captor;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;

import java.util.Random;

public class CPUCaptor {
    private DoubleProperty cpuLoad = new SimpleDoubleProperty();
    private Random r = new Random();

    public CPUCaptor() {
    }

    public void refreshLoadAverage() {
        cpuLoad.setValue((r.nextInt(100)/100d));
    }

    public DoubleProperty getCpuLoad() {
        return cpuLoad;
    }
}
